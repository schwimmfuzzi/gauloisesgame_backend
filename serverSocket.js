// app.js
var express = require('express');
var app = express();
var http = require('http');



app.set('port', process.env.PORT || 3001);
var server = http.createServer(app);
var io = require('socket.io').listen(server);
server.listen(app.get('port'));

io.on('connection', function(client) {
    console.log('Client connected...');

    client.on('join', function(data) {
        console.log(data);
		client.emit('messages', 'Hello from server');
    });

});


server.listen(3100);
