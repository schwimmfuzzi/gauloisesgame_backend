var config = require('./config.js');

module.exports = function(app, routes) {

    app.get('/', function(req, res) {
        return res.json('version: ' + config.version)
    });

    app.get(config.base + '/quizes/rand', routes.quizes.rand);
    app.get(config.base + '/quizes/:id', routes.quizes.show);
    app.post(config.base + '/quizes/custom', routes.quizes.updateCustom);
    app.post(config.base + '/quizes/all', routes.quizes.updateAll);
    app.post(config.base + '/quizes/:id', routes.quizes.update);
    app.get(config.base + '/quizes', routes.quizes.list);

    app.patch(config.base + '/buzzers', routes.buzzers.update);
    app.get(config.base + '/buzzers', routes.buzzers.list);

    app.patch(config.base + '/points', routes.points.update);
    app.get(config.base + '/points', routes.points.list);

    app.patch(config.base + '/settings', routes.settings.update);
    app.get(config.base + '/settings', routes.settings.list);

    app.patch(config.base + '/stats', routes.stats.update);
    app.get(config.base + '/stats', routes.stats.list);

    app.post(config.base + '/images/single/custom', routes.images.showCustom);
    app.post(config.base + '/images/single', routes.images.show);
    app.get(config.base + '/images/custom', routes.images.listCustom);
    app.get(config.base + '/images', routes.images.list);
};
