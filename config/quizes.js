[
  {
    "title": "12 Monkeys",
    "path": "12Monkeys.png",
    "active": true
  },
  {
    "title": "27 Dresses",
    "path": "27Dresses.png",
    "active": true
  },
  {
    "title": "Aliens Die Rückkehr",
    "path": "AliensDieRueckkehr.png",
    "active": true
  },
  {
    "title": "American Beauty",
    "path": "AmericanBeauty.png",
    "active": true
  },
  {
    "title": "American Pie",
    "path": "AmericanPie.png",
    "active": true
  },
  {
    "title": "Ant Man",
    "path": "AntMan.png",
    "active": true
  },
  {
    "title": "Arche Noah",
    "path": "ArcheNoah.png",
    "active": true
  },
  {
    "title": "Black Hawk Down",
    "path": "BlackHawkDown.png",
    "active": true
  },
  {
    "title": "Blood Diamant",
    "path": "BloodDiamant.png",
    "active": true
  },
  {
    "title": "Braveheart",
    "path": "BraveHeart.png",
    "active": true
  },
  {
    "title": "Broke Back Mountain",
    "path": "BrokeBackMountain.png",
    "active": true
  },
  {
    "title": "Casino Royale",
    "path": "CasinoRoyale.png",
    "active": true
  },
  {
    "title": "Cats And Dogs",
    "path": "CatsAndDogs.png",
    "active": true
  },
  {
    "title": "Cat Woman",
    "path": "CatWoman.png",
    "active": true
  },
  {
    "title": "Chocolat",
    "path": "Chocolat.png",
    "active": true
  },
  {
    "title": "Das Boot",
    "path": "DasBoot.png",
    "active": true
  },
  {
    "title": "Das Dschungelbuch",
    "path": "DasDschungelbuch.png",
    "active": true
  },
  {
    "title": "Das Geisterhaus",
    "path": "DasGeisterhaus.png",
    "active": true
  },
  {
    "title": "Das grosse Krabbeln",
    "path": "DasGrosseKrabbeln.png",
    "active": true
  },
  {
    "title": "Das letzte Einhorn",
    "path": "DasLetzteEinhorn.png",
    "active": true
  },
  {
    "title": "Das Phantom der Oper",
    "path": "DasPhantomDerOper.png",
    "active": true
  },
  {
    "title": "Das Schweigen der Lämmer",
    "path": "DasSchweigenDerLaemmer.png",
    "active": true
  },
  {
    "title": "Der Herr der Ringe",
    "path": "DerHerrDerRinge.png",
    "active": true
  },
  {
    "title": "Der König der Löwen",
    "path": "DerKoenigDerLoewen.png",
    "active": false
  },
  {
    "title": "Der mit dem Wolf tanzt",
    "path": "DerMitDemWolfTanzt.png",
    "active": false
  },
  {
    "title": "Der Teufel trägt Prada",
    "path": "DerTeufelTraegtPrada.png",
    "active": true
  },
  {
    "title": "Dirty Dancing",
    "path": "DirtyDancing.png",
    "active": true
  },
  {
    "title": "Eat Pray Love",
    "path": "EatPrayLove.png",
    "active": false
  },
  {
    "title": "Edward mit den Scherenhänden",
    "path": "EdwardMitDenScherenhaenden.png",
    "active": false
  },
  {
    "title": "Eiskalte Engel",
    "path": "EiskalteEngel.png",
    "active": false
  },
  {
    "title": "ET",
    "path": "ET.png",
    "active": false
  },
  {
    "title": "Findet Nemo",
    "path": "FindetNemo.png",
    "active": false
  },
  {
    "title": "Forrest Gump",
    "path": "ForrestGump.png",
    "active": false
  },
  {
    "title": "Free Willy",
    "path": "FreeWilly.png",
    "active": false
  },
  {
    "title": "French Kiss",
    "path": "FrenchKiss.png",
    "active": false
  },
  {
    "title": "Ghostbusters",
    "path": "Ghostbusters.png",
    "active": false
  },
  {
    "title": "Harry Potter",
    "path": "HarryPotter.png",
    "active": false
  },
  {
    "title": "Heartbreaker",
    "path": "Heartbreaker.png",
    "active": false
  },
  {
    "title": "Honig im Kopf",
    "path": "HonigImKopf.png",
    "active": false
  },
  {
    "title": "Into the Wild",
    "path": "IntoTheWild.png",
    "active": false
  },
  {
    "title": "James Bond",
    "path": "JamesBond.png",
    "active": false
  },
  {
    "title": "Keinohrhase",
    "path": "Keinohrhase.png",
    "active": false
  },
  {
    "title": "King Kong",
    "path": "KingKong.png",
    "active": false
  },
  {
    "title": "Knocking on Heavens Door",
    "path": "KnockingOnHeavensDoor.png",
    "active": false
  },
  {
    "title": "Kung Fu Panda",
    "path": "KungFuPanda.png",
    "active": false
  },
  {
    "title": "Lammbock",
    "path": "Lammbock.png",
    "active": false
  },
  {
    "title": "Les Miserables",
    "path": "LesMiserables.png",
    "active": false
  },
  {
    "title": "Life of Pie",
    "path": "LifeOfPie.png",
    "active": false
  },
  {
    "title": "Little Miss Sunshine",
    "path": "LittleMissSunshine.png",
    "active": false
  },
  {
    "title": "Midnight In Paris",
    "path": "MidnightInParis.png",
    "active": false
  },
  {
    "title": "Million Dollar Baby",
    "path": "MillionDollarBaby.png",
    "active": false
  },
  {
    "title": "Nicht Auflegen",
    "path": "NichtAuflegen.png",
    "active": false
  },
  {
    "title": "No Country for Old Men",
    "path": "NoCountryForOldMen.png",
    "active": false
  },
  {
    "title": "Oben",
    "path": "Oben.png",
    "active": false
  },
  {
    "title": "Per Anhalter durch die Galaxy",
    "path": "PerAnhalterDurchDieGalaxy.png",
    "active": false
  },
  {
    "title": "Planet der Affen",
    "path": "PlanetDerAffen.png",
    "active": false
  },
  {
    "title": "Police Academy",
    "path": "PoliceAcademy.png",
    "active": false
  },
  {
    "title": "Ratatouille",
    "path": "Ratatouille.png",
    "active": false
  },
  {
    "title": "Robocop",
    "path": "Robocop.png",
    "active": false
  },
  {
    "title": "Sherlock Holmes",
    "path": "SherlockHolmes.png",
    "active": false
  },
  {
    "title": "Spiderman",
    "path": "Spiderman.png",
    "active": false
  },
  {
    "title": "Taxi Taxi",
    "path": "TaxiTaxi.png",
    "active": false
  },
  {
    "title": "Ted",
    "path": "Ted.png",
    "active": false
  },
  {
    "title": "The Ring",
    "path": "TheRing.png",
    "active": false
  },
  {
    "title": "The Wolf of Wallstreet",
    "path": "TheWolfOfWallStreet.png",
    "active": false
  },
  {
    "title": "Thor",
    "path": "Thor.png",
    "active": false
  },
  {
    "title": "Tiger & Dragon",
    "path": "TigerandDragon.png",
    "active": false
  },
  {
    "title": "Titanic",
    "path": "Titanic.png",
    "active": false
  },
  {
    "title": "Transformers",
    "path": "Transformers.png",
    "active": false
  },
  {
    "title": "Vom Winde verweht",
    "path": "VomWindeVerweht.png",
    "active": false
  },
  {
    "title": "Xmen",
    "path": "XMen.png",
    "active": false
  },
  {
    "title": "Zeitgeist",
    "path": "Zeitgeist.png",
    "active": false
  },
  {
    "title": "Zurück in die Zukunft",
    "path": "ZurueckInDieZukunft.png",
    "active": false
  },
  {
    "title": "Zweiohrküken",
    "path": "Zweiohrkueken.png",
    "active": false
  }
]
