var config = {};

// port on api listens
config.port = 3001;
config.version = 'v1';
config.base = '/' + config.version;

config.name = 'gauloises game backend';
// access origin for CORS stuff
config.origin = '*';

config.settings = {
    default: 'config/settings_default.json',
    custom: 'config/settings_custom.json'
};

config.quizes = {
    default: {
        list: 'config/quizes.js'
    },
    custom: {
        list: 'config/customQuizes.js'
    },
    pathCustom: 'gauloises'

}

module.exports = config;
