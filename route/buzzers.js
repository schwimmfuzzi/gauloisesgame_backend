// this file is just for bootstrapping the different controllers

var buzzers = {
    update: require('./buzzers/update.js'),
    list: require('./buzzers/list.js')
};

exports.update = buzzers.update.update;
exports.list = buzzers.list.list;
