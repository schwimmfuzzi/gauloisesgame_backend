var config = require('../../config/config.js');
var stats = require('../../config/stats.js');
var jsonfile = require('jsonfile');

function random(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

exports.rand = function(req, res) {

	var quizListDefault = jsonfile.readFileSync(config.quizes.default.list);
	var quizListCustom = jsonfile.readFileSync(config.quizes.custom.list);

	var quizes = [];

	var tmp = {};
	for(var i = 0; i<quizListDefault.length; i++) {
		if(quizListDefault[i].active) {
			tmp = quizListDefault[i];
			tmp.source = 'default'
			quizes.push(tmp);
		}
	}

	for(var i = 0; i<quizListCustom.length; i++) {
		if(quizListCustom[i].active) {
			tmp = quizListCustom[i];
			tmp.source = 'custom'
			quizes.push(tmp);
		}
	}


    if (stats.nextQuiz) {
        stats.nextQuiz = false;
    }

	if (typeof stats.answerFlag != 'undefined') {
		// console.log('unser answer flags');
		delete stats.answerFlag;
	}

    var curQuizNumber = stats.currentQuiz;
    var newQuizNumber = -1;

    var quiz = quizes[curQuizNumber];

    // enforcing to not show the same quiz twice
    do {
        newQuizNumber = random(0, quizes.length);
        stats.currentQuiz = newQuizNumber;
    } while (curQuizNumber == newQuizNumber)

    return res.json(200, quizes[newQuizNumber]);
};
