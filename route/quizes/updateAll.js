var config = require('../../config/config.js');
var stats = require('../../config/stats.js');
var jsonfile = require('jsonfile');

exports.updateAll = function(req, res) {

  var quizes = req.body.quizes;


	// save quiz list.
	jsonfile.writeFile(config.quizes.default.list, quizes, {spaces: 2}, function(err) {
		 if(err)
			return res.json(500, 'problems while writing settings');

			return res.json(200, quizes);
	});


};
