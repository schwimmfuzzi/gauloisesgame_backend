var config = require('../../config/config.js');
var jsonfile = require('jsonfile');

exports.list = function(req, res) {

		var quizList = jsonfile.readFileSync(config.quizes.default.list);

    return res.json(200, quizList);

};
