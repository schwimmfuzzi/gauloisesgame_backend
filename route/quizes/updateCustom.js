var config = require('../../config/config.js');
var stats = require('../../config/stats.js');
var jsonfile = require('jsonfile');

exports.updateCustom = function(req, res) {

  var quizList = req.body.quizList;

	// save quiz list.
	jsonfile.writeFile(config.quizes.custom.list, quizList, {spaces: 2}, function(err) {
		 if(err)
			return res.json(500, 'problems while writing settings');

			return res.json(200, quizList);
	});


};
