var config = require('../../config/config.js');
var stats = require('../../config/stats.js');
var jsonfile = require('jsonfile');

exports.update = function(req, res) {
	var quizList = jsonfile.readFileSync(config.quizes.default.list);

  var quiz = req.body.quiz;
	var qid = req.params.id;

  quizList[qid] = quiz;

	// save quiz list.
	jsonfile.writeFile(config.quizes.default.list, quizList, {spaces: 2}, function(err) {
		 if(err)
			return res.json(500, 'problems while writing settings');

			return res.json(200, quiz);
	});


};
