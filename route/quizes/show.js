var config = require('../../config/config.js');
var stats = require('../../config/stats.js');

var jsonfile = require('jsonfile');

function random(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

exports.show = function(req, res) {
	var quizList = jsonfile.readFileSync(config.quizes.default.list);
    var id = req.params.id;
	var quizes = [];


	for(var i = 0; i<quizList.length; i++) {
		if(quizList[i].active) {
				quizes.push(quizList[i]);
		}
	}

    var quiz = quizes[id];

    return res.json(200, quiz);
};
