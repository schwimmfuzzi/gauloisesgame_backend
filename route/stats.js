// this file is just for bootstrapping the different controllers

var stats = {
    update: require('./stats/update.js'),
    list: require('./stats/list.js')
};

exports.update = stats.update.update;
exports.list = stats.list.list;
