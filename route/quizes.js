// this file is just for bootstrapping the different controllers

var quizes = {
    rand: require('./quizes/rand.js'),
    show: require('./quizes/show.js'),
    list: require('./quizes/list.js'),
    update: require('./quizes/update.js'),
    updateAll: require('./quizes/updateAll.js'),
    updateCustom: require('./quizes/updateCustom.js')
};

exports.rand = quizes.rand.rand;
exports.show = quizes.show.show;
exports.list = quizes.list.list;
exports.update = quizes.update.update;
exports.updateAll = quizes.updateAll.updateAll;
exports.updateCustom = quizes.updateCustom.updateCustom;
