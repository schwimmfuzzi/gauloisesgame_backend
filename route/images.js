// this file is just for bootstrapping the different controllers

var images = {
	show: require('./images/show.js'),
    list: require('./images/list.js'),
    listCustom: require('./images/listCustom.js'),
    showCustom: require('./images/showCustom.js')
};

exports.show = images.show.show;
exports.list = images.list.list;
exports.listCustom = images.listCustom.listCustom;
exports.showCustom = images.showCustom.showCustom;
