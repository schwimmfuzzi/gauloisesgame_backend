// this file is just for bootstrapping the different controllers

var settings = {
    update: require('./settings/update.js'),
    list: require('./settings/list.js')
};

exports.update = settings.update.update;
exports.list = settings.list.list;
