var config = require('../../config/config.js');
var jsonfile = require('jsonfile');

exports.update = function (req, res) {
	jsonfile.writeFile(config.settings.custom, req.body.settings, {spaces: 2}, function(err) {
		 if(err)
		 	return res.json(500, 'problems while writing settings');
	});

      return res.json(200, req.body.settings);
};
