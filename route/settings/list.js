var config = require('../../config/config.js');
var jsonfile = require('jsonfile');
var _ = require('underscore');


exports.list = function(req, res) {

	var defaults = jsonfile.readFileSync(config.settings.default);
	var custom = jsonfile.readFileSync(config.settings.custom);

	if(_.isEqual(defaults, custom))
		return res.json(200, defaults);
	else
		return res.json(200, custom);
};
