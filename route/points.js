// this file is just for bootstrapping the different controllers

var points = {
    update: require('./points/update.js'),
    list: require('./points/list.js')
};

exports.update = points.update.update;
exports.list = points.list.list;
