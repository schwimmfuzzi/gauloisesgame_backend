var points = require('../../config/points.js');

exports.update = function(req, res) {

    var p = req.body.points;

    points.p1 = p.p1;
    points.p2 = p.p2;
    points.p3 = p.p3;

    return res.json(200, points);
};
