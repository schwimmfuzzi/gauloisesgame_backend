var fs = require('fs');
var jsonfile = require('jsonfile');
var walk = require('walk');
var config = require('../../config/config.js');
var files = [];

// getting home directory, depending on platform
function getUserHome() {
    return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
}
// getting default image directory, depending on platform
function getUserPictures() {
    return (process.platform == 'win32') ? '\\Pictures\\' : '/Pictures/';
}

function pushIfNew(candidate, existing) {
    var exists = false;
    for (var i = 0, len = existing.length; i < len; i++) {
        var e = existing[i];
        if (e.path == candidate.name) {
			files.push(e);
            return;
        }
    }

    if (!exists) {
		if(!candidate.name.indexOf('.DS_Store') && !candidate.name.indexOf('Thumbs.db')) {
	        files.push({
	            "path": candidate.name,
	            "title": "",
	            "active": true
	        });
		} else {
			// console.log('hidden file: ', candidate.name);
		}
    }
}


exports.listCustom = function(req, res) {

    var quizList = jsonfile.readFileSync(config.quizes.custom.list);

    files = [];
    var path = getUserHome() + getUserPictures() + config.quizes.pathCustom;

    // Walker options
    var walker = walk.walk(path, {
        followLinks: false
    });

    walker.on('file', function(root, stat, next) {
        // Add this file to the list of files
        pushIfNew(stat, quizList)
        next();
    });


    walker.on('end', function() {
        return res.json(200, files);
    });


};
