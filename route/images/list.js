var fs = require('fs');

var walk    = require('walk');
var files   = [];


exports.list = function(req, res) {
	files = [];

	// Walker options
	var walker  = walk.walk('images', { followLinks: false });

	walker.on('file', function(root, stat, next) {
	    // Add this file to the list of files
	    files.push(stat.name);
		next();
	});

	walker.on('end', function() {
	    return res.json(200, files);
	});


};
