var fs = require('fs');
var config = require('../../config/config.js');

// getting home directory, depending on platform
function getUserHome() {
  return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
}
// getting default image directory, depending on platform
function getUserPictures() {
  return (process.platform == 'win32') ? '\\PICTURES\\' : '/Pictures/';
}


exports.showCustom = function(req, res) {

	var image = req.body.image;
	var path = getUserHome() + getUserPictures() + config.quizes.pathCustom + '/';

    fs.readFile(path + image.filename, function(err, data) {
        if(err) {
			res.json(500, err);
		} else {
			res.writeHead(200, {
	            'Content-Type': 'image/png'
	        });
	        res.end(new Buffer(data).toString('base64')); // Send the file data to the browser.
		}
    });
};
