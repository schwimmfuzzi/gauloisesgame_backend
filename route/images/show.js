var fs = require('fs');


exports.show = function(req, res) {

	var image = req.body.image;
    fs.readFile('images/' + image.filename, function(err, data) {
        if(err) {
			res.json(500, err);
		} else {
			res.writeHead(200, {
	            'Content-Type': 'image/png'
	        });
	        res.end(new Buffer(data).toString('base64')); // Send the file data to the browser.
		}
    });
};
