var stats = require('../../config/stats.js');
var io = require('socket.io');

exports.update = function(req, res) {

    var s = req.body.stats;
    stats.gamePaused = s.gamePaused;
    stats.pausingPlayer = s.pausingPlayer;
    stats.nextQuiz = s.nextQuiz;
    stats.answerFlag = s.answerFlag;
    stats.abortGame = s.abortGame;
    stats.path = s.path;
    stats.source = s.source;
    stats.title = s.title;

    return res.json(200, stats);
};
