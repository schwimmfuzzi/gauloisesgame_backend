var express = require('express');
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser'); //bodyparser + json + urlencoder
var morgan = require('morgan'); // logger
var config = require('./config/config.js');
var multipart = require('connect-multiparty');
var server = require('http').Server(app);


// .listen(server);


app.use(bodyParser());
app.use(morgan());
app.use(multipart());
app.use(cors());
app.listen(config.port);

//Routes
var routes = {
    quizes: require('./route/quizes.js'),
	buzzers: require('./route/buzzers.js'),
	points: require('./route/points.js'),
	settings: require('./route/settings.js'),
	stats: require('./route/stats.js'),
	images: require('./route/images.js')
};

app.all('*', function (req, res, next) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Credentials', true);
    res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH');
    res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization, ext_ip');
    if ('OPTIONS' == req.method) return res.send(200);
    next();
});

// falls es mal gebraucht wird.
//var checkAdmin = adminCheck();

// inside the routes, use checkAdmin only after the verifyToken =)
require('./config/routes')(app, routes);

console.log(config.name + ' is starting on port ' + config.port);
